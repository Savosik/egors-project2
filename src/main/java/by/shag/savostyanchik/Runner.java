package by.shag.savostyanchik;

import by.shag.savostyanchik.model.PrintedProduct;
import by.shag.savostyanchik.model.ProductService;
import ch.qos.logback.core.spi.ContextAware;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;
import java.util.stream.Stream;

@SpringBootApplication
public class Runner implements ApplicationContextAware {
    private static ApplicationContext context;


    public static void main(String[] args) {
        SpringApplication.run(Runner.class);
//        Map<String, PrintedProduct> beansOfType = context.getBeansOfType(PrintedProduct.class);
//        beansOfType.values()
//                .forEach(System.out::println);


        //System.out.println(beansOfType);

        context.getBean(ProductService.class).shawAll();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}