package by.shag.savostyanchik.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//@Component
public class Book implements PrintedProduct {
    private int versionBook;

    @Value("${printing.book.showMainPage}")
    private String mainPage;

    @Value("${printing.book.getPageSize}")
    private int pageSize;

    @Value("${printing.book.getName}")
    private String name;

    @Override
    public void showMainPage() {
        System.out.println("Main page: " + mainPage);
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public String getName() {
        return "Name: " + name;
    }

    @Override
    public void setVersion(int version) {
        versionBook = version;
    }

    @Override
    public String toString() {
        return "Book{" +
                "version book='" + versionBook +
                ", mainPage='" + mainPage +
                ", pageSize=" + pageSize +
                ", name='" + name +
                '}';
    }
}