package by.shag.savostyanchik.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    @Qualifier("versionBook3")
    private Book book;

    @Autowired
    @Qualifier("versionMagazine2")
    private Magazine magazine;

    @Autowired
    @Qualifier("versionNewspaper1")
    private Newspaper newspaper;

    public void shawAll() {
        System.out.println(book);
        System.out.println(magazine);
        System.out.println(newspaper);
    }
}
