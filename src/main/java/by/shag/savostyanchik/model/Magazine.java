package by.shag.savostyanchik.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//@Component
public class Magazine implements PrintedProduct {
    private int versionMagazine;

    @Value("${printing.magazine.showMainPage}")
    private String mainPage;

    @Value("${printing.magazine.getPageSize}")
    private int pageSize;

    @Value("${printing.magazine.getName}")
    private String name;

    @Override
    public void showMainPage() {
        System.out.println("Main page: " + mainPage);
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public String getName() {
        return "Name: " + name;
    }

    @Override
    public void setVersion(int version) {
        versionMagazine = version;
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "version magazine='" + versionMagazine +
                ", mainPage='" + mainPage +
                ", pageSize=" + pageSize +
                ", name='" + name +
                '}';
    }
}
