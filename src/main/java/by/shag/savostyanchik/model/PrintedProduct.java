package by.shag.savostyanchik.model;

public interface PrintedProduct {

    void showMainPage();

    int getPageSize();

    String getName();

    void setVersion(int version);
}
