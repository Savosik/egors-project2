package by.shag.savostyanchik.model;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Library {

    @Bean
    public Book versionBook1() {
        Book book = new Book();
        book.setVersion(1);
        return book;
    }


    @Bean
    public Book versionBook2() {
        Book book = new Book();
        book.setVersion(2);
        return book;
    }


    @Bean
    public Book versionBook3() {
        Book book = new Book();
        book.setVersion(3);
        return book;
    }


    @Bean
    public Magazine versionMagazine1() {
        Magazine magazine = new Magazine();
        magazine.setVersion(1);
        return magazine;
    }

    @Bean
    public Magazine versionMagazine2() {
        Magazine magazine = new Magazine();
        magazine.setVersion(2);
        return magazine;
    }

    @Bean
    public Newspaper versionNewspaper1() {
        Newspaper newspaper = new Newspaper();
        newspaper.setVersion(1);
        return newspaper;
    }
}
