package by.shag.savostyanchik.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//@Component
public class Newspaper implements PrintedProduct {
    private int versionNewspaper;

    @Value("${printing.newspaper.showMainPage}")
    private String mainPage;

    @Value("${printing.newspaper.getPageSize}")
    private int pageSize;

    @Value("${printing.newspaper.getName}")
    private String name;

    @Override
    public void showMainPage() {
        System.out.println("Main page: " + mainPage);
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }

    @Override
    public String getName() {
        return "Name: " + name;
    }

    @Override
    public void setVersion(int version) {
        versionNewspaper = version;
    }

    @Override
    public String toString() {
        return "Newspaper{" +
                "version newspaper='" + versionNewspaper +
                ", mainPage='" + mainPage +
                ", pageSize=" + pageSize +
                ", name='" + name +
                '}';
    }
}
